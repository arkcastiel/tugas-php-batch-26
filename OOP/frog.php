<?php

    require_once('animal.php');

    class Frog extends Animal{

        public $name;
        public function __construct($hewan){
            $this->name = $hewan;
        } 

        public function jump(){
            echo "Jump : hop hop";
        } 

    }

?>