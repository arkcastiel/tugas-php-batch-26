<?php

function xo($str) {
    $count_x = 0;
    $count_o = 0;

    $panjang = strlen($str); // 8
    for ($i=0; $i <= $panjang-1; $i++) { 
        if($str[$i] == "x"){
            $count_x++;
        }else{
            $count_o++;
        }
       
    }

    if ($count_x == $count_o) {
        return "Benar  <br>";
    } else {
        return "Salah <br>";
    }

    // Looping isi dari $str
    // cek apakah dia menggandung kata x, jika ada, count_x++ else count_o++
    //
    // cek apakah count_x == count_y, jika ya return "bEnar" else " slah"
    //Code disini
}

// Test Cases

echo xo('xoxoxo'); // "Benar"
echo xo('oxooxo'); // "Salah"
echo xo('oxo'); // "Salah"
echo xo('xxooox'); // "Benar"
echo xo('xoxooxxo'); // "Benar"

?>