<?php

/* Buatlah sebuah function dengan nama pagar_bintang yang menerima parameter angka. 
function tersebut akan mereturn string yang membentuk sebuah "#" jika di baris ganjil dan "*" jika dibaris genap*/

function pagar_bintang($integer){
    $baris = "";
    $kolom = "";
    for ($i=0; $i<=$integer; $i++){
    $baris .= $i;
        for ($j=0; $j < $integer; $j++) {
        $kolom .= $i;
            if ($i%2 == 0){
                return $kolom . "# <br>";
            } else {
                return $kolom . "* <br>";
            }
        }
        return $baris ."<br>";
    }
    return "<br>";
    //cek integernya itu ganjil apa genap, jika ganjil maka varialbelnya isi dengan string # else isi dengan *
    //code disini
}

echo pagar_bintang(5);
echo pagar_bintang(8);
echo pagar_bintang(10);

?>